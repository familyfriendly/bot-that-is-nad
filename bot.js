const Discord = require('discord.js')
const Enmap = require('enmap')

console.log(`Starting ${require('./config').name}`)



// Client
const client = new Discord.Client()

// Modules
const modules = new Discord.Collection()

// Exports
module.exports = { client, modules }

// Module loader
const getFileList = require('./library/getFileList')
getFileList('./modules').forEach(moduleName => {
    const module = require(moduleName)
    modules.set(module.meta.name, module)
})

// Module autorun
modules
    .filter(module => module.meta.autorun)
    .sort((a, b) => a.meta.autorun - b.meta.autorun)
    .forEach(module => module.run())
