const { client } = require('../../bot')

exports.run = (m, a) => {
    // Create an embed
    const embed = client.helpers.embed()
        .setDescription(`Pong! ${Math.floor(client.ping)} ms :ping_pong:`)

    // Send the embed
    m.channel.send({embed})
}

exports.meta = {
    names: ['ping', 'pong'],
    permissions: [],
    help: {
        description: 'See the bot\'s ping',
        usage: '',
        category: 'help'
    }
}