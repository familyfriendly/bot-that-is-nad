const Preset = require("../../library/models/presets")
const { client } = require('../../bot')
exports.run = (m, a) => {
   
const template = {
    creds: m.member.tag,
    roles: m.guild.roles.map(r => {
        return {
            name: r.name,
            id: r.id,
            color: r.hexColor,
            perms: r.permissions,
            hoist: r.hoist,
        }
    }),
    channels: m.guild.channels.filter(c => c.type === "category").map(c => {
        return {
            name:c.name,
            position:c.position,
            perms: c.permissionOverwrites.filter(p => p.type === "role"),
            children: c.children.map(child => {
                return {
                    name:child.name,
                    type:child.type,
                    position:child.position,
                    perms: child.permissionOverwrites.filter(p => p.type === "role")
                }
            })
        }
    })
}
let meta = {
    tags:[],
    name:"",
    serverID:m.guild.id,
    description:"",
    username:m.member.tag
}



client.helpers.prompt(m,"what do you want to name this preset?",20)
.then(a => {
    if(!a) return;
    meta.name = a.first().content.toLowerCase()
    client.helpers.prompt(m,'what tags do you want to give this preset? (separate with ",")',30)
    .then(a2 => {
        if(!a2) return;
        meta.tags = a2.first().content.toLowerCase().split(",")
        client.helpers.prompt(m,'give a description of your server',40)
        .then(a3 => { 
            meta.description = a3.first().content.toLowerCase()
            const preset = new Preset({
                username: meta.username,
                serverID: meta.serverID,
                name: meta.name,
                description: meta.description,
                tag1:meta.tags[0],
                tag2:meta.tags[1],
                tag3:meta.tags[2],
                preset: template
            
            })
            
            preset.save()
            .then(result => {
                let embed = client.helpers.embed('GREEN')
                embed.addField(`preset succesfully added to database!`,`**preset id:** ${result._id}`)
                m.channel.send({embed})
            })
            .catch(err => console.error(err))
         })
    })
})






}

exports.meta = {
    names: ['submit', 'upload'],
    permissions: ["ADMINISTRATOR"],
    help: {
        description: 'submit a preset',
        usage: '',
        category: 'main'
    }
}