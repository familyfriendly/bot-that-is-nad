# preset structure.

```js
const template = {
    roles: m.guild.roles.map(r => {
        return {
            name: r.name,
            id: r.id,
            color: r.hexColor,
            perms: r.permissions,
            hoist: r.hoist,
        }
    }),
    channels: m.guild.channels.filter(c => c.type === "category").map(c => {
        return {
            name:c.name,
            position:c.position,
            perms: c.permissionOverwrites.filter(p => p.type === "role"),
            children: c.children.map(child => {
                return {
                    name:child.name,
                    type:child.type,
                    position:child.position,
                    perms: child.permissionOverwrites.filter(p => p.type === "role")
                }
            })
        }
    })
}
```