# oppy 
This bot is using a bot boilerplate made by @barkingdog.
you can find the boilerplate info [HERE](#discord-bot-boilerplate).
I will not guide you through how to set up oppy but it should be pretty easy to figure out.
# links
* **add the bot:** https://discordapp.com/api/oauth2/authorize?client_id=487576643272048673&permissions=8&redirect_uri=https%3A%2F%2Ffamilyfriendly.xyz%2Fb%2Fbob%2Freadme&response_type=code&scope=bot
* **vote on dbl:** https://discordbots.org/bot/487576643272048673


# docs
 # Help
 ## Dev
 ### Eval
 - Run code
 - Aliases: `e`
 - Usage: `eval someCode()`
 - You need `BOT OWNER` to run this command
 ### Gendocs
 - Generates help documentation in markdown
 - You need `BOT OWNER` to run this command
 ### Reload
 - Reload the commands without restarting the bot
 - You need `BOT OWNER` to run this command
 ### Say
 - Make the bot say something
 - Aliases: `talk`
 - Usage: `say text`
 - You need `BOT OWNER` to run this command
 ### Silenteval
 - Run code silently
 - Aliases: `se`
 - Usage: `silenteval someCode()`
 - You need `BOT OWNER` to run this command
 ### Stop
 - Stop the bot
 - You need `BOT OWNER` to run this command
 ## Help
 ### Help
 - See the list of all commands
 ### Ping
 - See the bot's ping
 - Aliases: `pong`
 ## Main
 ### Preset
 - load a preset
 - Aliases: `load`
 - Usage: `preset  <preset ID>`
 - You need `ADMINISTRATOR` to run this command
 ### Submit
 - submit a preset
 - Aliases: `upload`
 - You need `ADMINISTRATOR` to run this command





# Discord bot boilerplate

[![MIT](https://img.shields.io/badge/License-MIT-blue.svg?style=flat-square)](https://gitlab.com/BarkingDog/barking-bot/blob/master/LICENSE.md)
[![NODE](https://img.shields.io/badge/Language-node.js-brightgreen.svg?style=flat-square)](https://nodejs.org/en/)
[![SUPPORTSERVER](https://img.shields.io/badge/Support%20server-Join-yellow.svg?style=flat-square)](https://discord.gg/N8Fqcuk)
[![gitlab repo](https://img.shields.io/badge/gitlab%20repo-gitlab/BarkingDog-brightgreen.svg?style=flat-square)](https://gitlab.com/BarkingDog/discord-bot-boilerplate)


Included features:
- Command loader
- Module loader
- A database
- Custom prefix
- Basic sharding (very basic)
- Help command generator
- Some example commands
- Lots of documentation

## Where do I start?
Read [Usage](DOCS/usage.md) for an **install and basic usage guide**.

Check out [DOCS](DOCS/) for full documentation.

## Support
Discord [support server](https://discord.gg/N8Fqcuk)

Add me: `BarkingDog#4975`
