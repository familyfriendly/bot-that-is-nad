const { client } = require('../../bot')
const config = require('../../config')

exports.run = () => {
    console.log('Modules and commands have been loaded\nLogging in\n')
    client.login(config.tokens.discord)

    client.on('ready', () => {
        console.log(
            '##################################################\n' +
            `# ${config.name} was started\n` + 
            `# In ${client.guilds.size} guilds with ${client.users.size} users\n` +
            `# The owners are: ${config.owners.map(id => client.users.get(id).tag).join(', ')}\n` +
            `# The default prefix is ${config.defaultPrefix}\n` +
            '##################################################\n'
        )

        const activity = () => config.activity.text
            .replace(/BOTservers/g, client.guilds.size)
            .replace(/BOTprefix/g, config.defaultPrefix)
            .replace(/BOTusers/g, client.users.size)

            client.user.setActivity(activity(), config.activity)
        setInterval(() => {
            client.user.setActivity(activity(), config.activity)
        }, 60 * 1000)
    })
}

exports.meta = {
    name: 'login',
    autorun: 2
}