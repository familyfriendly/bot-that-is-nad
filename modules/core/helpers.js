const Discord = require('discord.js')
const config = require('../../config')
const { client } = require('../../bot')

exports.run = () => {
    client.helpers = {}

    // Embed
    client.helpers.embed = col => new Discord.RichEmbed().setColor(col || config.embedColor)
    // Sleep
    client.helpers.sleep = ms => new Promise(res => setTimeout(res, ms))

    // Total guild count
    client.helpers.guildCount = async () => {
        if (client.shard) {
            const values = await client.shard.fetchClientValues('guilds.size')
            return values.reduce((p, v) => p + v, 0)
        } else return client.guilds.size
    }
    // Total user count
    client.helpers.userCount = async () => {
        if (bot.shard) {
            const values = await client.shard.fetchClientValues('users.size')
            return values.reduce((p, v) => p + v, 0)
        } else return client.users.size
    }

    client.helpers.prompt = async (message,question,rt) => {
        
        let embed = client.helpers.embed()
        embed.addField(question,`question terminated ${rt} seconds after this message is sent...`)
        let questionMessage = await message.channel.send({embed})
        let r = await message.channel.awaitMessages(response => response.author.id === message.author.id, { max: 1, time: rt * 1000, errors: ['time'] })
                .catch(e => {
                    embed = client.helpers.embed('RED')
                    embed.addField(`question "${question}" failed`,'something went wrong.')
                    questionMessage.delete()
                    message.channel.send({embed})
                    return 
                })
        return r
    }

    client.helpers.compile = (structure) => {
        return {
            channels: structure.channels.map(c => {
                return {
                    name: c.cat_name,
                    perms: c.cat_perms.filter(p => p.type === "role"),
                    position:null,
                    children: c.channels.map(child => {
                        return {
                            name: child.name,
                            type: child.type,
                            position:null,
                            perms: child.perms.filter(p => p.type === "role")
                        }
                    })
                }
            }),
            roles: structure.roles
        }
    }

}

exports.meta = {
    name: 'helpers',
    autorun: 1
}